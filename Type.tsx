export type Planet = {
    date: string;
    explanation: string;
    title: string;
    url: string;
}