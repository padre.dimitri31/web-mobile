import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import axios from 'axios';
import { Planet } from '../Type';
import PlanetItem from '../Components/PlanetItem';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { FlatList } from 'react-native-gesture-handler';
import PlanetList from '../Components/PlanetList';
import { Platform } from 'react-native'

type Props = {
	navigation: any;
}



const Home: React.FC<Props> = ({ navigation }) => {

    const [planets, setPlanets] = useState<Planet[]>([])
    const [ViewPlanet, setViewPlanet] = useState<Planet>(planets[planets.length - 1]);
    let dateEnd = format(new Date(),"yyyy-MM-dd");
    let date = subDays(new Date(),7);
    let dateStart = format(date,"yyyy-MM-dd");

    

    const asyncFunction = async () => {
        const res = await axios.get(`https://api.nasa.gov/planetary/apod?api_key=JaiKy5Ng7Z5vCJ3VDlIsNA364g5bDfnvb7H2gIfx&start_date=${dateStart}&end_date=${dateEnd}`)
        setPlanets(res.data);
    }

    React.useEffect(()=>{
        asyncFunction();
    }, []);
    const PlanetSorted = planets.sort((a,b) => a.date < b.date ? 1:-1)

    return(
        <View style={styles.container}>

            <View style={styles.containerTop}>
                <FlatList
                    data={PlanetSorted}
                    renderItem={({item}) =>
                    <TouchableOpacity
                        onPress={() => setViewPlanet(item) 
                        }
                    >
                         <PlanetList planet={item}/>
                    </TouchableOpacity>
                }  
                    keyExtractor={galaxie => galaxie.date}
                    horizontal={true}
        
                />
            </View>
            <View style={styles.containerBot}>
                <PlanetItem planet= {ViewPlanet?ViewPlanet:PlanetSorted[0]} navigation={navigation}>
                    
                </PlanetItem>
            </View>
        </View>
            
        
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'space-between',
        backgroundColor: '#000000',
      },
    containerTop: {
        flex: 0.15,
        backgroundColor: '#000000',
        marginTop:20,
        alignItems:"center",
      },
    containerBot: {
        flex: 1,
        backgroundColor: '#000000',
      },
      droidSafeArea: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? 25 : 0
    },
});

export default Home