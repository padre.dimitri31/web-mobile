import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import Home from '../Screens/Home';
import { Planet } from '../Type';

type Props = {
    route: any;
    navigation: any;
}

const zoomPlanet: React.FC<Props>  = ({route,navigation}) => {
    const{Planet} = route.params;
     return(
        
        <ImageBackground 
            source={{uri : Planet?.url}}
            style={{width: "100%", height: "100%"}}
        >
        </ImageBackground>
    )   
}
export default zoomPlanet