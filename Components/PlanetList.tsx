import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import Home from '../Screens/Home';
import { Planet } from '../Type';
import PlanetItem from './PlanetItem';

type Props = {
    planet: Planet | undefined
}


const PlanetList: React.FC<Props>  = ({planet}) => {
    
    return(
        // <>
                <View style={styles.box} >
                    
                    <ImageBackground 
                        source={{uri : planet?.url}}
                        style={ styles.image} 
                        imageStyle={{ borderRadius: 15}}
                    >
                    </ImageBackground>                
                </View>
        //</>
    );
}

const styles = StyleSheet.create({
    box: {
        width:80,
        height:80,
        margin: 5,
    },
    image: {
        width:"100%",
        height:"100%",
        borderRightColor:"#9370DB",
        borderRightWidth: 2,
        borderLeftColor:"#9370DB",
        borderLeftWidth: 2,
        borderBottomColor:"#9370DB",
        borderBottomWidth:2,
        borderTopColor:"#9370DB",
        borderTopWidth: 2,
        borderRadius: 18,
    },
});

export default PlanetList