import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Planet } from '../Type';

type Props = {
    planet: Planet | undefined
    navigation: any
}

const PlanetItem: React.FC<Props>  = ({planet, navigation}) => {
    
    return(
        <View style={styles.container} >
            <TouchableOpacity onPress={() => {
					navigation.navigate('zoomPlanet', {
                        Planet: planet,
					});
				}}>
           
                <ImageBackground 
                    source={{uri : planet?.url}}
                    style={{width: "100%", height: "100%"}} 
                >
                <LinearGradient 
               colors={['transparent', '#00000000', '#000000']}
                start={{ x: 0, y: 1 }}
                end={{ x: 0, y: 0 }}
                >
                <View style={{height:"5%"}}></View>
                </LinearGradient>
                <LinearGradient 
                colors={['transparent', '#020024', '#000000']}
                style={{
                    position: 'absolute', 
                    left: 0,
                    right: 0,
                    bottom: 0,
                    //height: 300,
                }}
                start={{ x: 0, y: 0 }}
                end={{ x: 0, y: 1 }}
                >

                <Text style={styles.title}>
                    {planet?.title}
                </Text>
                <Text style={styles.explanation}>
                    {planet?.explanation}
                </Text>
                </LinearGradient>
                </ImageBackground>
            </TouchableOpacity>
        </View>
        
    );
}

const styles = StyleSheet.create({
    container: {
        width:"100%",
        height:"100%",
      },
    title: {
        fontWeight: '800',
        fontSize: 42,
        padding: 16,
        color:"white",
    },
    explanation: {
        padding: 16,
        color:"white",
    },
});

export default PlanetItem